const std = @import("std");
const fmt = std.fmt;

const Buffer = std.ArrayList(u8);

pub const RGB = struct {
    r: usize,
    g: usize,
    b: usize,
};

pub const Style = struct {
    const Self = @This();

    buf: *Buffer,

    pub fn init(buf: *Buffer) Self {
        return .{ .buf = buf };
    }

    pub fn setString(self: *Self, str: []const u8) !void {
        try self.buf.appendSlice(str);
    }

    pub fn fmtString(self: *Self, comptime fmts: []const u8, args: anytype) !void {
        var buf: [22]u8 = undefined;
        const str = try fmt.bufPrint(&buf, fmts, args);
        try self.buf.appendSlice(str);
    }

    pub fn setHexBg(self: *Self, hex: u32) !void {
        const c: RGB = hexToRGB(hex);
        try self.fmtString("\x1b[48;2;{d};{d};{d}m", .{ c.r, c.g, c.b });
    }

    pub fn reset(self: *Self) void {
        self.buf.appendSlice("\x1b[0m") catch unreachable;
    }

    pub fn slice(self: *Self) []const u8 {
        self.reset();
        return self.buf.constSlice();
    }
};

fn hexToRGB(hex: u32) RGB {
    var rgb: RGB = undefined;

    rgb.r = (hex & 0xFF0000) >> 16;
    rgb.g = (hex & 0x00FF00) >> 8;
    rgb.b = (hex & 0x0000FF) >> 0;

    return rgb;
}
