const std = @import("std");
const log = std.log;
const mem = std.mem;
const ps = std.process;
const fs = std.fs;
const os = std.os;
const io = std.io;

const BUFFER_SIZE = 1024 * 10;
var ALLOC_BUFFER: [BUFFER_SIZE]u8 = undefined;

const String = std.ArrayList(u8);

const RawTerm = struct {
    const Self = @This();

    // Reseting the terminal
    termios: os.termios,
    handle: os.system.fd_t,

    fn init(handle: fs.File.Handle) Self {
        const old_term = try os.tcgetattr(handle);
        var term = old_term;
        term.lflag &= ~(os.system.ECHO | os.system.ICANON);
        try os.tcsetattr(handle, .FLUSH, term);
        return .{ .termios = old_term, .handle = handle };
    }

    fn disableRawMode(self: *Self) !void {
        try os.tcsetattr(self.handle, .FLUSH, self.termios);
    }
};

const Editor = struct {
    const Self = @This();

    doc: String,

    fn init(ally: mem.Allocator) Self {
        return .{ .doc = String.init(ally) };
    }

    fn deinit(self: *Self) void {
        self.doc.deinit();
    }
};

pub fn main() void {
    var allocator = std.heap.FixedBufferAllocator.init(&ALLOC_BUFFER);
    var ally = allocator.allocator();

    var cmd_args = ps.args();
    _ = cmd_args.next();

    const file_name = cmd_args.next() orelse {
        log.err("Please provide a file.", .{});
        ps.exit(1);
    };

    const file = fs.cwd().openFile(file_name, .{}) catch {
        log.err("Could not open {s}", .{file_name});
        ps.exit(1);
    };

    defer file.close();

    var editor = Editor.init(ally);
    defer editor.deinit();

    const stat = file.stat() catch unreachable;
    const file_reader = file.reader();

    file_reader.readAllArrayList(&editor.doc, stat.size) catch unreachable;

    const stdout_writer = io.getStdOut().writer();
    var buffered_stdout = io.bufferedWriter(stdout_writer);
    const stdout = buffered_stdout.writer();

    stdout.writeAll(editor.doc.items) catch unreachable;
    buffered_stdout.flush() catch unreachable;
}
