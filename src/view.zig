const std = @import("std");
const mem = std.mem;
const io = std.io;
const fs = std.fs;
const os = std.os;

const BUFFER_SIZE = 1024 * 4;

const FileWriter = fs.File.Writer;
const FileReader = fs.File.Reader;
const BuffWriter = io.BufferedWriter(BUFFER_SIZE, FileWriter);

pub const Str = std.ArrayList(u8);

pub const Position = struct {
    x: usize,
    y: usize,
};

pub const Event = union(enum) {
    key: u8,
};

pub const Size = struct {
    row: u16,
    col: u16,
};

pub const Cmd = union(enum) {
    Move: Position,
    None,
    Quit,
};

pub fn Tui(comptime T: type) type {
    return struct {
        const Self = @This();

        ally: mem.Allocator,
        stdin: FileReader,
        stdout: BuffWriter,
        quit: bool,
        cursor: Position,

        pub fn init(ally: mem.Allocator) Self {
            const stdin = io.getStdIn().reader();

            const stdout_writer = io.getStdOut().writer();
            const buffered_writer = io.bufferedWriter(stdout_writer);

            var self: Self = undefined;
            self.ally = ally;
            self.stdin = stdin;
            self.stdout = buffered_writer;
            self.cursor = .{ .x = 1, .y = 1 };
            self.quit = false;

            return self;
        }

        pub fn run(self: *Self, model: T) !void {
            var frame = Str.init(self.ally);
            defer frame.deinit();

            const stdout = self.stdout.writer();
            var m = model;

            while (!self.quit) {
                try ansi.clear(stdout);
                try ansi.move_cursor(stdout, .{ .x = 1, .y = 1 });

                m.view(&frame);
                try self.print(frame.items);

                try ansi.move_cursor(stdout, self.cursor);
                try self.flush();
                frame.clearRetainingCapacity();

                const event = try self.nextInput();
                const cmd = m.update(event);
                self.handleCmd(cmd);
            }
        }

        fn handleCmd(self: *Self, cmd: Cmd) void {
            switch (cmd) {
                .None => {},
                .Quit => self.quit = true,
                .Move => |pos| self.cursor = pos,
            }
        }

        fn print(self: *Self, str: []const u8) !void {
            const stdout = self.stdout.writer();
            try stdout.print("{s}", .{str});
        }

        fn flush(self: *Self) !void {
            try self.stdout.flush();
        }

        // TODO: handle utf-8
        fn nextInput(self: Self) !u8 {
            var buf: [20]u8 = undefined;
            const ch = try self.stdin.read(&buf);
            _ = ch;
            return buf[0];
        }
    };
}

const RawTerm = struct {
    const Self = @This();

    // Reseting the terminal
    termios: os.termios,
    handle: os.system.fd_t,

    pub fn disableRawMode(self: *Self) !void {
        try os.tcsetattr(self.handle, .FLUSH, self.termios);
    }
};

pub fn getRawTerm(stdin_file: fs.File) !RawTerm {
    const old_term = try os.tcgetattr(stdin_file.handle);
    var term = old_term;

    term.lflag &= ~(os.system.ECHO | os.system.ICANON);

    try os.tcsetattr(stdin_file.handle, .FLUSH, term);

    return .{
        .termios = old_term,
        .handle = stdin_file.handle,
    };
}

const ansi = struct {
    fn clear(writer: anytype) !void {
        try writer.print("\x1b[2J", .{});
    }
    fn move_cursor(writer: anytype, pos: Position) !void {
        try writer.print("\x1b[{}:{}H", .{ pos.y, pos.x });
    }
    fn block_cursor(writer: anytype) !void {
        try writer.print("\x1b[\x32 q", .{});
    }
    fn blinking_cursor(writer: anytype) !void {
        try writer.print("\x1b[\x35 q", .{});
    }
    fn getSize(fd: std.os.fd_t) !Size {
        var ws: os.system.winsize = undefined;

        const err = os.system.ioctl(fd, os.system.T.IOCGWINSZ, @ptrToInt(&ws));

        if (os.errno(err) == .SUCCESS) return Size{ .row = ws.ws_row, .col = ws.ws_col };
        return error.terminalSize;
    }
};
