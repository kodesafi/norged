const std = @import("std");
const io = std.io;
const os = std.os;
const fs = std.fs;
const ps = std.process;

const vn = @import("view.zig");
const st = @import("state.zig");
const ui = @import("style.zig");

const BUFFERSIZE = 1024 * 10;
var global_buf: [BUFFERSIZE]u8 = undefined;

const Mode = enum {
    Insert,
    Normal,
};

const Editor = struct {
    const Self = @This();

    state: st.State,
    cursor: usize,
    offset: usize,
    mode: Mode,

    fn setOffset(self: *Self) void {
        const curr = self.state.lineAt(self.cursor) orelse unreachable;
        self.offset = self.cursor -| curr.from;
    }

    fn moveRight(self: *Self) void {
        const last_char = self.state.constSlice().len - 1;
        self.cursor = @min(last_char, self.cursor + 1);
        self.setOffset();
    }

    fn moveLeft(self: *Self) void {
        self.cursor -|= 1;
        self.setOffset();
    }

    fn moveUp(self: *Self) void {
        const curr = self.state.lineAt(self.cursor) orelse unreachable;
        if (self.state.getLine(curr.number -| 1)) |prev| {
            const new = @min(prev.from + self.offset, prev.to);
            if (self.state.lineAt(new)) |_| {
                self.cursor = new;
            }
        }
    }

    fn moveDown(self: *Self) void {
        const curr = self.state.lineAt(self.cursor) orelse unreachable;
        if (self.state.getLine(curr.number + 1)) |next| {
            const new = @min(next.from + self.offset, next.to);
            if (self.state.lineAt(new)) |_| {
                self.cursor = new;
            }
        }
    }

    fn normal_update(self: *Self, ch: u8) void {
        switch (ch) {
            'i' => self.mode = .Insert,
            'l' => self.moveRight(),
            'h' => self.moveLeft(),
            'j' => self.moveDown(),
            'k' => self.moveUp(),

            else => {},
        }
    }

    fn insert_update(self: *Self, ch: u8) void {
        switch (ch) {
            '\x1b' => self.mode = .Normal,
            '\x7f' => {
                self.moveLeft();

                var trans: st.Transaction = undefined;
                trans.modify = .{
                    .opn = .Delete,
                    .idx = self.cursor,
                    .chr = ch,
                };

                self.state.update(trans) catch unreachable;
            },
            else => {
                var trans: st.Transaction = undefined;
                trans.modify = .{
                    .opn = .Insert,
                    .idx = self.cursor,
                    .chr = ch,
                };

                self.state.update(trans) catch unreachable;
                self.moveRight();
            },
        }
    }

    pub fn update(self: *Self, ch: u8) vn.Cmd {
        if (ch == 'q' and self.mode == .Normal) return .Quit;

        switch (self.mode) {
            .Normal => self.normal_update(ch),
            .Insert => self.insert_update(ch),
        }

        var pos: vn.Position = undefined;
        const line = self.state.lineAt(self.cursor) orelse unreachable;

        pos.x = (self.cursor - line.from) + 1;
        pos.y = line.number;

        return .{ .Move = pos };
    }

    // TODO: range should follow cursor.
    pub fn view(self: *Self, str: *vn.Str) void {
        const lines = self.state.lines();

        var style = ui.Style.init(str);

        var i: usize = 1;

        while (i <= lines) : (i += 1) {
            const line = self.state.getLine(i).?;
            style.setString(line.text) catch unreachable;
        }

        switch (self.mode) {
            .Normal => style.setString("\n -- NORMAL --") catch unreachable,
            .Insert => style.setString("\n -- INSERT --") catch unreachable,
        }
    }
};

pub fn main() !void {
    var buf = std.heap.FixedBufferAllocator.init(&global_buf);
    var alloc = buf.allocator();

    var args = ps.args();
    _ = args.next();

    const file_name = args.next() orelse {
        std.log.err("Please provide a file.", .{});
        ps.exit(1);
    };

    const file = try fs.cwd().openFile(file_name, .{});

    var editor: Editor = undefined;
    editor.cursor = 0;
    editor.offset = 0;
    editor.mode = .Normal;
    editor.state = try st.newState(file, alloc);

    // Set raw mode
    const stdin = io.getStdIn();
    var term = try vn.getRawTerm(stdin);
    defer term.disableRawMode() catch {};

    var tui = vn.Tui(Editor).init(alloc);
    try tui.run(editor);
}
