const std = @import("std");
const log = std.log;
const mem = std.mem;
const fs = std.fs;

const Content = std.ArrayList(u8);
const Lines = std.ArrayList(usize);

const Operation = enum {
    Insert,
    Delete,
};

pub const Modify = struct {
    opn: Operation,
    idx: usize,
    chr: u8,
};

pub const Transaction = struct {
    selection: ?Selection,
    modify: ?Modify,
};

const Line = struct {
    from: usize,
    to: usize,
    number: usize,
    text: []const u8,
};

const Selection = struct {
    anchor: usize = 0,
    head: usize = 0,
    active: bool = false,

    fn from(self: Selection) usize {
        if (self.anchor <= self.head) return self.anchor;
        return self.head;
    }

    fn to(self: Selection) ?usize {
        if (self.anchor >= self.head) return self.anchor;
        if (self.active) return self.head;
        return null;
    }
};

pub const State = struct {
    const Self = @This();
    doc: Content,
    lns: Lines,
    selection: Selection,

    pub fn lines(self: Self) usize {
        return self.lns.items.len;
    }

    pub fn getLine(self: Self, idx: usize) ?Line {
        if (idx > self.lines() or idx == 0) return null;

        const lns = self.lns.items;
        const doc = self.doc.items;

        var line: Line = undefined;

        line.from = 0;
        line.to = lns[idx - 1];

        if (idx > 1) {
            line.from = lns[idx - 2] + 1;
            line.to = lns[idx - 1];
        }

        line.number = idx;
        line.text = doc[line.from .. line.to + 1];

        return line;
    }

    pub fn lineAt(self: Self, idx: usize) ?Line {
        if (idx >= self.constSlice().len) return null;
        const lns = self.lns.items;
        for (lns) |l, i| if (idx <= l) return self.getLine(i + 1);
        unreachable;
    }

    pub fn update(self: *Self, trans: Transaction) !void {
        if (trans.selection) |sel| self.selection = sel;

        if (trans.modify) |mod| switch (mod.opn) {
            .Insert => try self.doc.insert(mod.idx, mod.chr),
            .Delete => _ = self.doc.orderedRemove(mod.idx),
        };

        try self.updateLines();
    }

    pub fn constSlice(self: Self) []const u8 {
        return self.doc.items;
    }

    fn updateLines(self: *Self) !void {
        self.lns.clearRetainingCapacity();
        const doc = self.doc.items;

        for (doc) |ch, i| {
            if (ch != '\n' and i < doc.len - 1) continue;
            try self.lns.append(i);
        }
    }
};

pub fn newState(file: fs.File, alloc: mem.Allocator) !State {
    const stat = try file.stat();
    const file_reader = file.reader();

    var state: State = undefined;
    state.lns = Lines.init(alloc);
    state.selection = Selection{};

    var content = Content.init(alloc);
    try file_reader.readAllArrayList(&content, stat.size);

    state.doc = content;
    try state.updateLines();

    return state;
}
